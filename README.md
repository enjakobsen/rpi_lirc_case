# README #


### What is this repository for? ###

3D Printable case for the Raspberry Pi 3 with custom LIRC compatible circuit

### How do I get set up? ###

Repo contains two manifold COLLADA objects that can be printed using FDM methods.
Use Slic3r or your favorite slicing software to convert to g-code, load run and print.
Tested with a homebrew Prusa i3


### Who do I talk to? ###

enjakobsen at gmail dot com